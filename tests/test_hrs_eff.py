import pytest


@pytest.mark.parametrize(
    "heat_recovery_type,"
    "hrs_thermal_eff_en308,"
    "expected",
    [
        ("phex", 0.8, 210),
        ("rw", 0.78, 150),
        ("rac", 0.78, 300),
    ],
)
def test_heat_recovery_efficiency_bonus(
    heat_recovery_type: str,
    hrs_thermal_eff_en308: float,
    expected: float,
) -> float:
    from erp_air.erp_utils import hrs_eff_bonus

    assert hrs_eff_bonus(
        heat_recovery_type, hrs_thermal_eff_en308
    ) == pytest.approx(expected)
