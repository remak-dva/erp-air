import pytest


@pytest.mark.parametrize(
    "uvu_fan_power_input_nominal,expected",
    [(30000, 0.631), (31000, 0.631)],
)
def test_fan_eff_uvu_min(
    uvu_fan_power_input_nominal, expected
):
    from erp_air.erp_utils import fan_eff_uvu_min

    assert (
        fan_eff_uvu_min(uvu_fan_power_input_nominal)
        == expected
    )
